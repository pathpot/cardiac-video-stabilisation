################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Detector/FASTDetector.cpp \
../src/Detector/ManualDetector.cpp 

OBJS += \
./src/Detector/FASTDetector.o \
./src/Detector/ManualDetector.o 

CPP_DEPS += \
./src/Detector/FASTDetector.d \
./src/Detector/ManualDetector.d 


# Each subdirectory must supply rules for building sources it contributes
src/Detector/%.o: ../src/Detector/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I/usr/local/include -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


