################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Tracker/HybridTracker.cpp \
../src/Tracker/PTracker.cpp \
../src/Tracker/RTracker.cpp 

OBJS += \
./src/Tracker/HybridTracker.o \
./src/Tracker/PTracker.o \
./src/Tracker/RTracker.o 

CPP_DEPS += \
./src/Tracker/HybridTracker.d \
./src/Tracker/PTracker.d \
./src/Tracker/RTracker.d 


# Each subdirectory must supply rules for building sources it contributes
src/Tracker/%.o: ../src/Tracker/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I/usr/local/include -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


