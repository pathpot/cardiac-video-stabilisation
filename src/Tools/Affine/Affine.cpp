#include "Affine.h"

cv::Point2d ps::Affine::transform(cv::Point2d p, cv::Mat &m){
	if(m.rows != 2 || m.cols != 3){
		std::cerr << "Affine Matrix size must be 2x3!\n" << std::endl;
		throw "Affine Matrix size must be 2x3!";
	}

	cv::Point2d pout;

	pout.x = m.at<double>(0, 0) * p.x + m.at<double>(0, 1) * p.y + m.at<double>(0, 2);
	pout.y = m.at<double>(1, 0) * p.x + m.at<double>(1, 1) * p.y + m.at<double>(1, 2);

	return pout;
}

bool ps::Affine::estimateAffine(cv::Mat &src_control_points, cv::Mat &dst_control_points, cv::Mat &affine){
	int n = src_control_points.rows;

	std::vector<cv::Point2f> src_vec, dst_vec;

	for(int i=0;i<n;i++){
		src_vec.push_back(cv::Point2f((float)src_control_points.at<double>(i, 0), (float)src_control_points.at<double>(i, 1)));
		dst_vec.push_back(cv::Point2f((float)dst_control_points.at<double>(i, 0), (float)dst_control_points.at<double>(i, 1)));
	}

	affine = cv::estimateRigidTransform(src_vec, dst_vec, false);

	if(affine.rows < 2 || affine.cols < 3){
		std::cerr << "Use least square estimation!" << std::endl;
		estimateAffineLS(src_control_points, dst_control_points, affine);
		return false;
	}

	return true;
}

void ps::Affine::getStabilizedPatch(cv::Mat &frame, cv::Mat &out_patch, cv::Mat &affine, cv::Point2d rMin, cv::Point2d rMax){
	// warp
	cv::Mat dispFrame(frame.size(), frame.type());
	cv::warpAffine(frame, dispFrame, affine, frame.size());

	// crop
	cv::Range xRange, yRange;
	xRange.start = (int)round(rMin.x);
	xRange.end = (int)round(rMax.x + 1);
	yRange.start = (int)round(rMin.y);
	yRange.end = (int)round(rMax.y + 1);

	// show
	dispFrame(yRange, xRange).copyTo(out_patch);
}

void ps::Affine::oneTimeWarp(cv::Mat &_ref_frame, cv::Mat &ref_features, cv::Mat &_tgt_frame, cv::Mat &tgt_features, cv::Mat &output){
	cv::Mat ref_frame, tgt_frame;
	cv::cvtColor(_ref_frame, ref_frame, CV_BGR2GRAY);
	cv::cvtColor(_tgt_frame, tgt_frame, CV_BGR2GRAY);
	cv::Mat affine;
	estimateAffine(ref_features, tgt_features, affine);
	getStabilizedPatch(tgt_frame, output, affine, cv::Point2d(0, 0), cv::Point2d(tgt_frame.cols, tgt_frame.rows));
}

bool ps::Affine::estimateAffineLS(cv::Mat &src_control_points, cv::Mat &dst_control_points, cv::Mat &affine){
	int nPoints = src_control_points.rows;
	//build Q
	cv::Mat Q(2, nPoints, CV_64FC1);
	for(int p=0;p<nPoints;p++){
		for(int k=0;k<2;k++){
			Q.at<double>(k, p) = src_control_points.at<double>(p, k);
		}
	}

	//build P
	cv::Mat P = cv::Mat::ones(3, nPoints, CV_64FC1);
	for(int p=0;p<nPoints;p++){
		for(int k=0;k<2;k++){
			P.at<double>(k, p) = dst_control_points.at<double>(p, k);
		}
	}

	affine.release();
	affine = cv::Mat(2, 3, CV_64FC1);

	affine = Q * P.t() * (P * P.t()).inv();

	return true;
}
