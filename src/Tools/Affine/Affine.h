#ifndef _PS_AFFINE_H_
#define _PS_AFFINE_H_

#include <opencv2/opencv.hpp>

namespace ps{

class Affine{
public:
	static cv::Point2d transform(cv::Point2d p, cv::Mat &m);
	static bool estimateAffine(cv::Mat &src_control_points, cv::Mat &dst_control_points, cv::Mat &affine);
	static void getStabilizedPatch(cv::Mat &frame, cv::Mat &out_patch, cv::Mat &affine, cv::Point2d rMin, cv::Point2d rMax);

	static void oneTimeWarp(cv::Mat &ref_frame, cv::Mat &ref_features, cv::Mat &tgt_frame, cv::Mat &tgt_features, cv::Mat &output);

	 //least square approach
	static bool estimateAffineLS(cv::Mat &src_control_points, cv::Mat &dst_control_points, cv::Mat &affine);
};

}



#endif
