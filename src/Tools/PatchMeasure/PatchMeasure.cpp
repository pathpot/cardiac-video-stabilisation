#include "PatchMeasure.h"

double ps::PatchMeasure::computeNCC(cv::Mat &img1, double x1, double y1, cv::Mat &img2, double x2, double y2, int winSize){
	int halfWindow = (winSize - 1)/2;
	int x1i = (int)round(x1);
	int y1i = (int)round(y1);
	int x2i = (int)round(x2);
	int y2i = (int)round(y2);

	if(img1.type() != CV_8UC1 || img2.type() != CV_8UC1){
		std::cerr << "image type must be CV_8UC1!" << std::endl;
		return -1;
	}

	if(x1i - halfWindow < 0 || x1i + halfWindow >= img1.cols || y1i - halfWindow < 0 || y1i + halfWindow >= img1.rows ||
			x2i - halfWindow < 0 || x2i + halfWindow >= img2.cols || y2i - halfWindow < 0 || y2i + halfWindow >= img2.rows ||
			x1i < 0 || x1i >= img1.cols || y1i < 0 || y1i >= img1.rows ||
			x2i < 0 || x2i >= img2.cols || y2i < 0 || y2i >= img2.rows ){
		std::cerr << "window reach boundary of the image!" << std::endl;
		return -1;
	}
	// compute mean
	double mean1 = 0, mean2 = 0;
	int n = (2*halfWindow + 1)*(2*halfWindow + 1);
	for(int dy = -halfWindow; dy <= halfWindow; dy++){
		for(int dx = -halfWindow; dx <= halfWindow; dx++){
			//printf("Begin : %d %d %d %d\n", img1.cols, img1.rows, x1i + dx, y1i + dy);
			mean1 += img1.at<unsigned char>(y1i + dy, x1i + dx);
			//printf("Begin : %d %d %d %d\n", img2.cols, img2.rows, x2i + dx, y2i + dy);
			mean2 += img2.at<unsigned char>(y2i + dy, x2i + dx);
			//printf("End\n");

		}
	}
	mean1 /= n;
	mean2 /= n;

	// compute sd
	double sd1 = 0, sd2 = 0;
	for(int dy = -halfWindow; dy <= halfWindow; dy++){
		for(int dx = -halfWindow; dx <= halfWindow; dx++){
			sd1 += (img1.at<unsigned char>(y1i + dy, x1i + dx) - mean1)*(img1.at<unsigned char>(y1i + dy, x1i + dx) - mean1);
			sd2 += (img2.at<unsigned char>(y2i + dy, x2i + dx) - mean2)*(img2.at<unsigned char>(y2i + dy, x2i + dx) - mean2);
		}
	}

	sd1 = sqrt(sd1/n);
	sd2 = sqrt(sd2/n);


	// compute NCC
	double ncc = 0;
	for(int dy = -halfWindow; dy <= halfWindow; dy++){
		for(int dx = -halfWindow; dx <= halfWindow; dx++){
			ncc += (img1.at<unsigned char>(y1i + dy, x1i + dx) - mean1)*(img2.at<unsigned char>(y2i + dy, x2i + dx) - mean2);
		}
	}
	ncc = ncc/(sd1*sd2*n);

	return ncc;
}

double ps::PatchMeasure::computeSSD(cv::Mat &img1, double x1, double y1, cv::Mat &img2, double x2, double y2, int winSize){
	int halfWindow = (winSize - 1)/2;
	int x1i = (int)round(x1);
	int y1i = (int)round(y1);
	int x2i = (int)round(x2);
	int y2i = (int)round(y2);

	if(img1.type() != CV_8UC1 || img2.type() != CV_8UC1){
		std::cerr << "image type must be CV_8UC1!" << std::endl;
		return -1;
	}

	if(x1i - halfWindow < 0 || x1i + halfWindow >= img1.cols || y1i - halfWindow < 0 || y1i + halfWindow >= img1.rows ||
			x2i - halfWindow < 0 || x2i + halfWindow >= img2.cols || y2i - halfWindow < 0 || y2i + halfWindow >= img2.rows){
		std::cerr << "window reach boundary of the image!" << std::endl;
		return -1;
	}

	// compute SSD
	double ssd = 0, diff;
	int n = (2*halfWindow + 1)*(2*halfWindow + 1);
	for(int dy = -halfWindow; dy <= halfWindow; dy++){
		for(int dx = -halfWindow; dx <= halfWindow; dx++){
			diff = img1.at<unsigned char>(y1i + dy, x1i + dx) - img2.at<unsigned char>(y2i + dy, x2i + dx);
			ssd += diff*diff;
		}
	}

	ssd = sqrt(ssd/n);

	return ssd;
}
