#ifndef _PS_PATCHMEASURE_H_
#define _PS_PATCHMEASURE_H_

#include <opencv2/opencv.hpp>

namespace ps{
class PatchMeasure{
public:
	static double computeSSD(cv::Mat &img1, double x1, double y1, cv::Mat &img2, double x2, double y2, int winSize);
	static double computeNCC(cv::Mat &img1, double x1, double y1, cv::Mat &img2, double x2, double y2, int winSize);
};

}


#endif
