#include "OpticalFlow.h"

// prev_img(A) to img(B)
void ps::OpticalFlow::indTrack(cv::Mat &prev_img, cv::Mat &img, cv::Mat &P_in, cv::Mat &P_out, int winSize, int iter_no, int pyre_no, std::vector<bool> &valid, double converge_eps, bool predict){
	// Memory Allocation
	cv::Mat CurLvlA, CurLvlB, PrevLvlA, PrevLvlB;
	valid.clear();

	// Build Pyramid
	cv::Mat *Apyr = new cv::Mat[pyre_no];
	cv::Mat *Bpyr = new cv::Mat[pyre_no];
	for(int lvl=0;lvl<pyre_no;lvl++){
		// get image in this level
		if(lvl == 0){
			prev_img.convertTo(Apyr[lvl], CV_64FC1);
			img.convertTo(Bpyr[lvl], CV_64FC1);
		}else{
			cv::pyrDown(Apyr[lvl-1], Apyr[lvl]);
			cv::pyrDown(Bpyr[lvl-1], Bpyr[lvl]);
		}
	}

	// Set some parameters
	int halfWindow = (winSize - 1)/2;

	// initialize flow of each point
	cv::Mat Pflow_in;
	if(predict){
		Pflow_in = P_out - P_in;

	}else{ // not predict
		Pflow_in = cv::Mat::zeros(P_in.size(), P_in.type());
	}

	cv::Mat uv = cv::Mat::zeros(Pflow_in.size(), Pflow_in.type());
	cv::Mat Pflow = cv::Mat::zeros(Pflow_in.size(), Pflow_in.type());

	// loop variables
	cv::Mat P, Ix, Iy, A, B, H, It;
	double us, vs;
	double hAlpha = 0.001;
	int lvl_width, lvl_height;
	double this_x, this_y;

	for(int lvl = pyre_no-1;lvl >= 0; lvl--){
		// get level width and height
		lvl_width = Apyr[lvl].cols;
		lvl_height = Apyr[lvl].rows;

		// get a reflected image in this level
		imReflect( Apyr[lvl], A, halfWindow);
		imReflect( Bpyr[lvl], B, halfWindow);

		// initial without flow
		P = P_in/pow(2, lvl) + halfWindow;
        Pflow = Pflow_in/pow(2,lvl);

        // Loop for update flow of each point
        for(int pIdx = 0; pIdx < P_in.rows;pIdx++){
        	for(int iter = 0;iter < iter_no;iter++){
        		// calculate gradient and hessian (G)
        		indGradient(B,
        				P.at<double>(pIdx, 0) + Pflow.at<double>(pIdx, 0) + uv.at<double>(pIdx, 0),
        				P.at<double>(pIdx, 1) + Pflow.at<double>(pIdx, 1) + uv.at<double>(pIdx, 1),
        				halfWindow, Ix, Iy);

        		indHessian(Ix, Iy, hAlpha, H);

        		// calculate image difference
        		indID(A, B, It, P.at<double>(pIdx, 0), P.at<double>(pIdx, 1),
        				Pflow.at<double>(pIdx, 0) + uv.at<double>(pIdx, 0),
        				Pflow.at<double>(pIdx, 1) + uv.at<double>(pIdx, 1),
        				halfWindow);

        		// update
        		indLK(It, Ix, Iy, H, us, vs);

        		if(isnan(us) || isnan(vs) || fabs(us) > lvl_width || fabs(vs) > lvl_height){
        			std::cerr << "Skip incorrect flow updating at the boundary" << std::endl;
        			us = vs = 0;
        		}


        		// update parameter
        		uv.at<double>(pIdx, 0) = uv.at<double>(pIdx, 0) + us;
        		uv.at<double>(pIdx, 1) = uv.at<double>(pIdx, 1) + vs;
        	}

        	if(lvl == 0){
            	//check convergence
            	if(sqrt(us*us + vs*vs) >= converge_eps){
            		valid.push_back(false);
            	}else{
            		valid.push_back(true);
            	}
        	}
        }

        // mutiply parameter by 2
        if (lvl != 0){
        	uv = 2 * uv;
        }
	}

	// set output
    Pflow = Pflow + uv;
    P_out = P_in + Pflow;

	// Memory Deallocation
    delete[] Apyr;
    delete[] Bpyr;
}

void ps::OpticalFlow::indGradient(cv::Mat &M, double xp, double yp, int halfWindow, cv::Mat &Ix, cv::Mat &Iy){
	Ix.release();
	Iy.release();
	Ix = cv::Mat::zeros(2*halfWindow + 1, 2*halfWindow + 1, CV_64FC1);
	Iy = cv::Mat::zeros(2*halfWindow + 1, 2*halfWindow + 1, CV_64FC1);

	double x, y, dx, dy;
	for(int i=0;i<2*halfWindow+1;i++){
		for(int j=0;j<2*halfWindow+1;j++){
			x = i + (xp - halfWindow);
			y = j + (yp - halfWindow);
			dx = bilinInterp(M, x+1, y) - bilinInterp(M, x-1, y);
			dy = bilinInterp(M, x, y+1) - bilinInterp(M, x, y-1);

            Ix.at<double>(i, j) = dx/2;
            Iy.at<double>(i, j) = dy/2;
		}
	}
}

void ps::OpticalFlow::indHessian(cv::Mat &Ix, cv::Mat &Iy, double alpha, cv::Mat &H){
	H.release();
	H = cv::Mat::zeros(2, 2, CV_64FC1);

	for(int i=0;i<Ix.rows;i++){
		for(int j=0;j<Ix.cols;j++){
		    H.at<double>(0,0) += Ix.at<double>(i, j)*Ix.at<double>(i, j);
		    H.at<double>(1,1) += Iy.at<double>(i, j)*Iy.at<double>(i, j);
		    H.at<double>(0,1) += Ix.at<double>(i, j)*Iy.at<double>(i, j);
		}
	}
    H.at<double>(0,0) += alpha;
    H.at<double>(1,1) += alpha;
    H.at<double>(1,0) = H.at<double>(0,1);
}

void ps::OpticalFlow::indID(cv::Mat &A, cv::Mat &B, cv::Mat &It, double xp, double yp, double xflow, double yflow, int halfWindow){
	It.release();
    It = cv::Mat::zeros(2*halfWindow + 1, 2*halfWindow + 1, CV_64FC1);

    double x, y;
    for(int i=0;i<2*halfWindow;i++){
    	for(int j=0;j<2*halfWindow;j++){
            x = i + (xp - halfWindow);
            y = j + (yp - halfWindow);
            It.at<double>(i, j) = bilinInterp(A, x, y) - bilinInterp(B, x + xflow, y + yflow);

    	}
    }
}

void ps::OpticalFlow::indLK(cv::Mat &It, cv::Mat &Ix, cv::Mat &Iy, cv::Mat &H, double &us, double &vs){
	cv::Mat b = cv::Mat::zeros(2, 1, CV_64FC1);
	cv::Mat x = cv::Mat::zeros(2, 1, CV_64FC1);
	for(int i=0;i<Ix.rows;i++){
		for(int j=0;j<Ix.cols;j++){
		    b.at<double>(0,0) += It.at<double>(i, j) * Ix.at<double>(i, j);
		    b.at<double>(1,0) += It.at<double>(i, j) * Iy.at<double>(i, j);
		}
	}

    cv::solve(H, b, x);

    us = x.at<double>(0);
    vs = x.at<double>(1);
}

void ps::OpticalFlow::imReflect(cv::Mat &src, cv::Mat &dst, int bordSize){
	int nrow = src.rows;
	int ncol = src.cols;

	// allocate memory
	dst.release();
	dst = cv::Mat::zeros(src.rows + 2*bordSize, src.cols + 2*bordSize, src.type());

	// Center
	src.copyTo(dst(cv::Range(bordSize, src.rows + bordSize), cv::Range(bordSize, src.cols + bordSize)));

	// Left
	for(int i=bordSize;i<bordSize + nrow;i++){
		for(int j=0;j<bordSize;j++){
			dst.at<double>(i, j) = src.at<double>(i-bordSize, bordSize - j);
		}
	}

	// Right
	for(int i=bordSize;i<bordSize + nrow;i++){
		for(int j=ncol+bordSize; j< ncol + 2*bordSize;j++){
			dst.at<double>(i, j) = src.at<double>(i-bordSize, 2*ncol + bordSize - j - 1);
		}
	}


	// Top
	for(int i=0;i<bordSize;i++){
		for(int j=0;j<dst.cols;j++){
			dst.at<double>(i, j) = dst.at<double>(2*bordSize - i, j);
		}
	}

	// Bottom
	for(int i=nrow + bordSize; i <nrow + 2*bordSize;i++){
		for(int j=0;j<dst.cols;j++){
			dst.at<double>(i, j) = dst.at<double>(2*nrow + 2*bordSize - i - 1, j);
		}
	}
}


double ps::OpticalFlow::bilinInterp(cv::Mat &M, double x, double y){
    double v = 0;
    int x0 = (int)ceil(x);
    int y0 = (int)ceil(y);
    double ax = x - x0;
    double ay = y - y0;

    if(x0 >= 0 && x0 + 1 < M.cols && y0 >= 0 && y0 + 1 < M.rows){
        v = (1-ay)*(1-ax)*M.at<double>(y0, x0) +
        		ay*(1-ax)*M.at<double>(y0 + 1, x0) +
        		(1-ay)*ax*M.at<double>(y0, x0 + 1) +
        		ay*ax*M.at<double>(y0 + 1, x0 + 1);
    }

    return v;
}
