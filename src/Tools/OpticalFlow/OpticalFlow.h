#ifndef _PS_OPTICALFLOW_H_
#define _PS_OPTICALFLOW_H_

#include <opencv2/opencv.hpp>

namespace ps{

class OpticalFlow{
public:
	// track each point individually
	static void indTrack(cv::Mat &prev_img, cv::Mat &img, cv::Mat &P_in, cv::Mat &P_out, int winSize, int iter_no, int pyre_no, std::vector<bool> &valid, double converge_eps, bool predict);

private:
	// individual
	static void indGradient(cv::Mat &M, double xp, double yp, int halfWindow, cv::Mat &Ix, cv::Mat &Iy);
	static void indHessian(cv::Mat &Ix, cv::Mat &Iy, double alpha, cv::Mat &H);
	static void indID(cv::Mat &A, cv::Mat &B, cv::Mat &It, double xa, double ya, double xflow, double yflow, int halfWindow);
	static void indLK(cv::Mat &It, cv::Mat &Ix, cv::Mat &Iy, cv::Mat &H, double &us, double &vs);

	// dense

	// utilities
	static void imReflect(cv::Mat &src, cv::Mat &dst, int bordSize);
	static double bilinInterp(cv::Mat &M, double xp, double yp);
};

}

#endif
