#include "TPS.h"


double ps::TPS::TPSbasis(cv::Point2d x1, cv::Point2d x2){
	double s = cv::norm(x1 - x2);
	if(s < FLT_EPSILON)
		return 0;

	return s*s*log(s*s);
}

void ps::TPS::constructKS(cv::Mat &ref_control_points, cv::Mat &KS){
	// number of control points
	int n = ref_control_points.rows;

	cv::Mat K, Kinv;
	K = cv::Mat::zeros(n+3, n+3, CV_64FC1);

	// loop for set L on K
	cv::Point2d ci, cj;
	for(int i=0; i<n; i++){
		for(int j=i; j<n; j++){
			ci = cv::Point2d(ref_control_points.at<double>(i, 0), ref_control_points.at<double>(i, 1));
			cj = cv::Point2d(ref_control_points.at<double>(j, 0), ref_control_points.at<double>(j, 1));
			K.at<double>(i, j) = TPSbasis(ci, cj);
			K.at<double>(j, i) = K.at<double>(i, j);
		}
	}

	// loop for set P on K
	for(int i=0;i<n;i++){
		K.at<double>(i, n) = 1;
		K.at<double>(n, i) = K.at<double>(i, n);

		K.at<double>(i, 1 + n) = ref_control_points.at<double>(i, 0);
		K.at<double>(1 + n, i) = K.at<double>(i, 1 + n);

		K.at<double>(i, 2 + n) = ref_control_points.at<double>(i, 1);
		K.at<double>(2 + n, i) = K.at<double>(i, 2 + n);
	}

	Kinv = K.inv();

	// loop for set KS (K*)
	KS = cv::Mat(n+3, n, CV_64FC1);
	for(int i=0;i<n+3;i++){
		for(int j=0;j<n;j++){
			KS.at<double>(i, j) = Kinv.at<double>(i, j);
		}
	}
}

void ps::TPS::constructM(cv::Mat &ref_pixels, cv::Mat &ref_control_points, cv::Mat &M){
	// number of pixel in the patch
	int N = ref_pixels.rows;

	// number of control points
	int n = ref_control_points.rows;

	M = cv::Mat(N, n+3, CV_64FC1);

	// loop for set V on M
	cv::Point2d xi, cj;
	for(int i=0;i<N;i++){
		for(int j=0;j<n;j++){
			xi = cv::Point2d(ref_pixels.at<double>(i, 0), ref_pixels.at<double>(i, 1));
			cj = cv::Point2d(ref_control_points.at<double>(j, 0), ref_control_points.at<double>(j, 1));
			M.at<double>(i, j) = TPS::TPSbasis(xi, cj);
		}
	}

	// loop for set W on M
	for(int i=0;i<N;i++){
		M.at<double>(i, n) = 1;
		M.at<double>(i, n + 1) = ref_pixels.at<double>(i, 0);
		M.at<double>(i, n + 2) = ref_pixels.at<double>(i, 1);
	}
}

cv::Point2d ps::TPS::warp(int pix_index, cv::Mat &M, cv::Mat &KS, cv::Mat &control_points){
	cv::Mat Mx = M.row(pix_index) * KS * control_points.col(0);
	cv::Mat My = M.row(pix_index) * KS * control_points.col(1);

	return cv::Point2d(Mx.at<double>(0, 0), My.at<double>(0, 0));
}

void ps::TPS::showStabilizedPatch(std::string win_name, cv::Point2d pMin, cv::Point2d pMax, cv::Mat &M, cv::Mat &KS, cv::Mat &cur_frame, cv::Mat &ref_pixels, cv::Mat &cur_control_points, bool zoom){
	// get stabilized patch
	cv::Mat stabilized_frame;
	getStabilizedPatch(cur_frame, stabilized_frame, ref_pixels, cur_control_points, pMin, pMax, M, KS);

	// zoom
	if(zoom)
		pyrUp(stabilized_frame, stabilized_frame);

	// display
	cv::imshow(win_name, stabilized_frame);
}

void ps::TPS::getStabilizedPatch(cv::Mat &cur_frame, cv::Mat &out_patch, cv::Mat &ref_pixels, cv::Mat &cur_control_points, cv::Point2d pMin, cv::Point2d pMax, cv::Mat &M, cv::Mat &KS){
	// set patch width and height
	int org_width = (int)round(pMax.x - pMin.x + 1);
	int org_height = (int)round(pMax.y - pMin.y + 1);

	int N = ref_pixels.rows;

	// display stabilized video
	out_patch = cv::Mat(org_height, org_width, CV_8UC1);
	cv::Point2d refPix, projPix;
	unsigned char stabilized_color;

	int projX, projY;
	for(int i=0;i<N;i++){
		refPix = cv::Point2d(ref_pixels.at<double>(i, 0), ref_pixels.at<double>(i, 1));
		projPix = TPS::warp(i, M, KS, cur_control_points);

		projX = (int)round(projPix.x);
		projY = (int)round(projPix.y);

		if(projX >= 0 && projX < cur_frame.cols && projY >= 0 && projY < cur_frame.rows)
			stabilized_color = cur_frame.at<unsigned char>(projY, projX);
		else
			stabilized_color = 0;

		out_patch.at<unsigned char>((int)round(refPix.y - pMin.y), (int)round(refPix.x - pMin.x)) = stabilized_color;

	}

}

void ps::TPS::oneTimeWarp(cv::Mat &_ref_frame, cv::Mat &ref_features, cv::Mat &_tgt_frame, cv::Mat &tgt_features, cv::Mat &output){
	cv::Mat ref_frame, tgt_frame;
	cv::cvtColor(_ref_frame, ref_frame, CV_BGR2GRAY);
	cv::cvtColor(_tgt_frame, tgt_frame, CV_BGR2GRAY);

	// reference pixels
	int N = ref_frame.rows*ref_frame.cols;
	cv::Mat ref_pixels = cv::Mat(N, 2, CV_64FC1);

	int cnt = 0;
	for(int y = 0; y < ref_frame.rows; y++){
		for(int x = 0; x < ref_frame.cols; x++){
			ref_pixels.at<double>(cnt, 0) = x;
			ref_pixels.at<double>(cnt, 1) = y;
			cnt++;
		}
	}

	// warp
	cv::Mat KS, M;
	constructKS(ref_features, KS);
	constructM(ref_pixels, ref_features, M);

	getStabilizedPatch(tgt_frame, output, ref_pixels, tgt_features, cv::Point2d(0, 0), cv::Point2d(ref_frame.cols - 1, ref_frame.rows - 1), M, KS);
}
