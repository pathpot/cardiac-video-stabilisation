#ifndef _PS_TPS_H_
#define _PS_TPS_H_

#include <opencv2/opencv.hpp>
#include <float.h>

namespace ps{

class TPS{
public:
	static double TPSbasis(cv::Point2d x1, cv::Point2d x2);
	static void constructKS(cv::Mat &ref_control_points, cv::Mat &KS);
	static void constructM(cv::Mat &ref_pixels, cv::Mat &ref_control_points, cv::Mat &M);
	static cv::Point2d warp(int pix_index, cv::Mat &M, cv::Mat &KS, cv::Mat &control_points);
	static void showStabilizedPatch(std::string win_name, cv::Point2d pMin, cv::Point2d pMax, cv::Mat &M, cv::Mat &KS, cv::Mat &cur_frame, cv::Mat &ref_pixels, cv::Mat &cur_control_points, bool zoom = false);
	static void getStabilizedPatch(cv::Mat &cur_frame, cv::Mat &out_patch, cv::Mat &ref_pixels, cv::Mat &cur_control_points, cv::Point2d pMin, cv::Point2d pMax, cv::Mat &M, cv::Mat &KS);

	static void oneTimeWarp(cv::Mat &ref_frame, cv::Mat &ref_features, cv::Mat &tgt_frame, cv::Mat &tgt_features, cv::Mat &output);
};



}


#endif
