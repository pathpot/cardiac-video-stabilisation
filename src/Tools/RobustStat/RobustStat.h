#ifndef _PS_ROBUSTSTAT_H_
#define _PS_ROBUSTSTAT_H_

#include <opencv2/opencv.hpp>

namespace ps{

class RobustStat{
public:
	static bool fastMCD(cv::Mat &X, cv::Mat &Tout, cv::Mat &Sout, int n_init_hypo = 500, int n_selected_hypo = 10);
	static double robustDistance(cv::Mat &data, cv::Mat &T, cv::Mat &S);
	static bool isOutLier(cv::Mat &data, cv::Mat &T, cv::Mat &S, double cut_off_threshold = 2.7162);

private:
public:
	static void createHypo(cv::Mat &X, cv::Mat &T, cv::Mat &S, int h);
	static bool cStep(cv::Mat &X, int h, cv::Mat &T, cv::Mat &S);

	static void computeMeanCov(cv::Mat &X, std::vector<int> xIndex, cv::Mat &mean, cv::Mat &cov);
	static double getRelativeDistance(cv::Mat &p1, cv::Mat &p2, cv::Mat &S);
};

}


#endif
