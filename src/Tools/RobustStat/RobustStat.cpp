#include "RobustStat.h"

// will not handle the case of large sample size
bool ps::RobustStat::fastMCD(cv::Mat &X, cv::Mat &Tout, cv::Mat &Sout, int n_init_hypo, int n_selected_hypo){
	srand (time(NULL));

	// chi2inv
	double chi2inv_5 = 1.3863;
	double chi2inv_975 = 7.3778;

	// initialize parameter
	int n = X.rows;
	int h = (int)floor(0.75*n);

	if(h < 3)
		return false;

	// 1. run every init hypo for 2 steps
	cv::Mat *T = new cv::Mat[n_init_hypo];
	cv::Mat *S = new cv::Mat[n_init_hypo];
	std::vector< std::pair<double, int> > detPairVect; // (det, index)
	double temp_det;

	// run n_init_hypo times
	for(int i=0;i<n_init_hypo;i++){
		// construct initial subset
		createHypo(X, T[i], S[i], h);

		// carry out two C-steps
		for(int k=0;k<2;k++){
			 cStep(X, h, T[i], S[i]);
		}

		// keep det
		temp_det = cv::determinant(S[i]);

		detPairVect.push_back(std::make_pair(temp_det, i));
	}

	// 2. sort according to det
	std::sort(detPairVect.begin(), detPairVect.end());

	// 3. carry out C-Step on this best hypo until convergence
	int cindex, bestindex;
	double min_det;
	for(int i=0;i<n_selected_hypo;i++){
		cindex = detPairVect[i].second;
		while(!cStep(X, h, T[cindex], S[cindex])){};

		temp_det = cv::determinant(S[cindex]);

		if(i == 0 || min_det > temp_det){
			min_det = temp_det;
			bestindex = cindex;
		}
	}

	// 4. report solution with lowest value of det
	T[bestindex].copyTo(Tout);
	S[bestindex].copyTo(Sout);

	// 5. adjust for multivariate normal consistency
	// - calculate d of all points
	double *all_dist = new double[n];
	cv::Mat xrow;
	for(int i=0;i<n;i++){
		// calculate distance
		xrow = X.row(i);
		all_dist[i] = getRelativeDistance(xrow, Tout, Sout);
	}

	// - find median of the set of d
	std::sort(all_dist, all_dist + n);
	double median_dist;
	if(n%2 == 1){
		median_dist = all_dist[n/2];
	}else{
		median_dist = (all_dist[n/2 - 1] + all_dist[n/2])/2;
	}

	// - adjust S
	Sout = (median_dist*median_dist/chi2inv_5)*Sout;

	// 6. reweighting
	// - calculate weight of all points
	double *all_w = new double[n];
	double sum_w = 0;
	double temp_dist, temp_threshold = sqrt(chi2inv_975);
	for(int i=0;i<n;i++){
		// calculate distance
		xrow = X.row(i);
		temp_dist = getRelativeDistance(xrow, Tout, Sout);

		// get weight
		if(temp_dist <= temp_threshold){
			all_w[i] = 1;
			sum_w += 1;
		}else{
			all_w[i] = 0;
		}
	}

	// - set T
	Tout = Tout*0;
	for(int i=0;i<n;i++){
		xrow = X.row(i);
		Tout = Tout + all_w[i] * xrow;
	}
	Tout = Tout/sum_w;

	// -set S
	Sout = Sout*0;
	for(int i=0;i<n;i++){
		xrow = X.row(i);
		Sout = Sout + all_w[i] * (xrow - Tout).t() * (xrow - Tout);
	}
	Sout = Sout/(sum_w - 1);

	// Release memory
	for(int i=0;i<n_init_hypo;i++){
		T[i].release();
		S[i].release();
	}
	delete[] T;
	delete[] S;
	delete[] all_dist;
	delete[] all_w;

	return true;
}

void ps::RobustStat::createHypo(cv::Mat &X, cv::Mat &T, cv::Mat &S, int h){
	// initialize variables
	int n = X.rows;
	std::vector<int> xIndex;
	std::vector<int> allIndex;
	for(int i=0;i<n;i++){
		allIndex.push_back(i);
	}

	// 1. get point set
	int ri, r;
	double temp_det;
	while(true){
		//random Index
		ri = rand() % allIndex.size();

		r = allIndex[ri];
		allIndex.erase(allIndex.begin() + ri);

		// push into xIndex
		xIndex.push_back(r);

		if(xIndex.size() >= 3){
			// compute mean & cov
			computeMeanCov(X, xIndex, T, S);

			// compute determinant
			temp_det = cv::determinant(S);

			if(temp_det > DBL_EPSILON){
				break;
			}
		}
	}

	// 2. Compute distance
	std::vector< std::pair<double, int> > distPairVect;
	cv::Mat xrow;
	double temp_dist;

	for(int i=0;i<n;i++){
		// calculate distance
		xrow = X.row(i);
		temp_dist = getRelativeDistance(xrow, T, S);

		// push into vector
		distPairVect.push_back(std::make_pair(temp_dist, i));
	}

	// 3. Sort according to distance
	std::sort(distPairVect.begin(), distPairVect.end());

	// 4. select only first h
	xIndex.clear();
	for(int i=0;i<h;i++){
		xIndex.push_back(distPairVect[i].second);
	}

	// 5. Compute output T and S
	computeMeanCov(X, xIndex, T, S);

}

// return true if converge
bool ps::RobustStat::cStep(cv::Mat &X, int h, cv::Mat &T, cv::Mat &S){
	int n = X.rows;

	cv::Mat prevT, prevS;
	T.copyTo(prevT);
	S.copyTo(prevS);

	// 1. Compute relative distance of each point in X
	std::vector< std::pair<double, int> > distPairVect;
	cv::Mat xrow;
	double temp_dist;
	for(int i=0;i<n;i++){
		// calculate distance
		xrow = X.row(i);
		temp_dist = getRelativeDistance(xrow, T, S);

		// push into vect
		distPairVect.push_back(std::make_pair(temp_dist, i));
	}

	// 2. Sort point index according to its distance
	std::sort(distPairVect.begin(), distPairVect.end());

	// 3. Pick only best h points
	std::vector<int> xIndex;
	for(int i=0;i<h;i++){
		xIndex.push_back(distPairVect[i].second);
	}

	// 4. Compute new T and S
	computeMeanCov(X, xIndex, T, S);

	// 5.Check Convergence
	bool converge = true;
	for(int i=0;i<S.rows;i++){
		if(T.at<double>(0, i) - prevT.at<double>(0, i) > DBL_EPSILON){
			converge = false;
			break;
		}

		for(int j=0;j<S.cols;j++){
			if(S.at<double>(i, j) - prevS.at<double>(i, j) > DBL_EPSILON){
				converge = false;
				break;
			}

		}

		if(converge)
			break;
	}

	return converge;
}

void ps::RobustStat::computeMeanCov(cv::Mat &X, std::vector<int> xIndex, cv::Mat &mean, cv::Mat &cov){
	int p = X.cols;
	int nx = (int)xIndex.size();

	// compute mean
	mean.release();
	mean = cv::Mat::zeros(1, p, CV_64FC1);
	for(int ix = 0; ix < nx; ix++){
		for(int i=0;i<p;i++){
			mean.at<double>(0, i) += X.at<double>((int)xIndex[ix], i);
		}
	}

	// normalization
	for(int j=0;j<p;j++){
		mean.at<double>(0, j) /= nx;
	}


	// compute covariance
	cov.release();
	cov = cv::Mat::zeros(p, p, CV_64FC1);
	for(int ix = 0; ix < nx; ix++){
		for(int i=0;i<p;i++){
			for(int j=0;j<p;j++){
				cov.at<double>(i, j) += (X.at<double>((int)xIndex[ix], i) - mean.at<double>(0, i))*
						(X.at<double>((int)xIndex[ix], j) - mean.at<double>(0, j));
			}
		}
	}

	// normalization
	for(int i=0;i<p;i++){
		for(int j=0;j<p;j++){
			cov.at<double>(i, j) /= (nx - 1);
		}
	}

}

double ps::RobustStat::getRelativeDistance(cv::Mat &p1, cv::Mat &p2, cv::Mat &S){

	cv::Mat dist = (p1 - p2) * S.inv() * (p1 - p2).t();

	return sqrt(dist.at<double>(0, 0));
}

bool ps::RobustStat::isOutLier(cv::Mat &data, cv::Mat &T, cv::Mat &S, double cut_off_threshold){
	double dist = getRelativeDistance(data, T, S);
	return dist > cut_off_threshold;
}

double ps::RobustStat::robustDistance(cv::Mat &data, cv::Mat &T, cv::Mat &S){
	return getRelativeDistance(data, T, S);
}
