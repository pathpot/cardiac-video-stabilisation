#ifndef _PS_STABILISER_H_
#define _PS_STABILISER_H_

#include <fstream>
#include <time.h>
#include <opencv2/opencv.hpp>

#include "../Tracker/Tracker.h"
#include "../Warper/Warper.h"
#include "../Detector/Detector.h"
#include "../HighlightInpainter/HighlightInpainter.h"
#include "../Manifold/Manifold.h"
#include "../History/History.h"
#include "../Predictor/Predictor.h"
#include "../BadFeatureChecker/BadFeatureChecker.h"

namespace ps{
class Stabiliser{
public:
	Stabiliser(cv::Point2d &minCorner, cv::Point2d &maxCorner);

	// for configuration
	void addDetector(Detector *detector);
	void addTracker(Tracker *tracker);
	void addWarper(Warper *warper);
	void addManifold(Manifold *manifold);
	void addPredictor(Predictor *predictor);
	void addHistory(History *history);
	void addBadFeatureChecker(BadFeatureChecker *badFeatureChecker);
	void addHighlightInpainter(HighlightInpainter *inpainter);

	// for running
	bool process(cv::Mat &frame);

	// for output
	bool getCurrentFeatures(cv::Mat &features, std::vector<bool> &valid_features);
	bool getStabilisedFrame(cv::Mat &frame);

private:
	void constructGoodFeatures(cv::Mat &ref_features, cv::Mat &cur_features, std::vector<bool> &valid_features, cv::Mat &good_ref_features, cv::Mat &good_cur_features);

private:
	bool first_frame;

	cv::Mat ref_features; //set of control points in tracked patch in the reference frame (n x 2)
	cv::Mat cur_features; //set of control points in tracked patch in the current frame (n x 2)
	cv::Mat prev_features; //set of control points in tracked patch in the current frame (n x 2)
	std::vector<bool> valid_features, bad_checker_valid_features;

	// image variables
	cv::Mat prev_frame, cur_frame, ref_frame;
	cv::Mat stabilised_frame;

	// patch
	cv::Point2d rMin, rMax;

private:
	Detector *detector;
	Tracker *tracker;
	Warper *warper;
	Manifold *manifold;
	Predictor *predictor;
	History *history;
	BadFeatureChecker *badFeatureChecker;
	HighlightInpainter *inpainter;
};
}


#endif

