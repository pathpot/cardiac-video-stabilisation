#include "Stabiliser.h"

ps::Stabiliser::Stabiliser(cv::Point2d &minCorner, cv::Point2d &maxCorner){
	first_frame = true;
	detector = NULL;
	tracker = NULL;
	warper = NULL;
	manifold = NULL;
	predictor = NULL;
	history = NULL;
	badFeatureChecker = NULL;
	inpainter = NULL;

	rMin = minCorner;
	rMax = maxCorner;
}

bool ps::Stabiliser::process(cv::Mat &frame){
	if(detector == NULL){
		std::cerr << "Detector is not specified!" << std::endl;
		return false;
	}

	if(tracker == NULL){
		std::cerr << "Tracker is not specified!" << std::endl;
		return false;
	}

	if(manifold == NULL){
		std::cerr << "Manifold is not specified!" << std::endl;
		return false;
	}

	if(history == NULL){
		std::cerr << "History is not specified!" << std::endl;
		return false;
	}

	// set frame
	cv::cvtColor(frame, cur_frame, CV_BGR2GRAY);

	// convert frame to its embedded coordinate
	cv::Mat cur_manifold;
	manifold->projectImage(cur_frame, cur_manifold);

	if(first_frame){
		first_frame = false;

		// detect
		detector->detect(cur_frame, cur_features, rMin, rMax);

		// set reference
		cur_features.copyTo(ref_features);
		cur_frame.copyTo(ref_frame);

		// init warp
		if(warper != NULL){
			printf("Warping...\n");
			warper->init(rMin, rMax, ref_frame, ref_features);
			warper->warp(cur_frame, ref_features, stabilised_frame);
		}


		// init history
		history->init(ref_features.rows, manifold->getIntrinsicDim());

		// init valid
		for(int i=0;i<ref_features.rows;i++){
			valid_features.push_back(true);
		}
	}else{
		// process specular highlight
		cv::Mat painted_frame;
		if(inpainter != NULL){
			printf("Inpainting...\n");
			inpainter->segmentAndinpaint(frame, painted_frame);
			cv::cvtColor(painted_frame, painted_frame, CV_BGR2GRAY);
		}else{
			cur_frame.copyTo(painted_frame);
		}

		// predictor & tracker
		if(predictor != NULL && predictor->predictf(cur_manifold, history->feature_history, history->manifold_history, history->T, cur_features)){
			printf("Predicting...\n");
		}
		printf("Tracking...\n");
		tracker->process(ref_frame, ref_features, prev_frame, prev_features, painted_frame, cur_features, valid_features);

//		// exp remove this (write file)
//		std::ofstream ofile;
//		ofile.open("features_hyb_genseq2.txt", std::ios::out | std::ios::app);
//		ofile << cur_features.at<double>(4, 0) << " " << cur_features.at<double>(4, 1) << std::endl;
//		ofile.close();

//		// DELETE (filename)
//		double end_time = clock()*1.0/CLOCKS_PER_SEC;
//		std::ofstream ofile;
//		ofile.open("tracking_time_NCC_genseq2.txt", std::ios::out | std::ios::app);
//		ofile << end_time - start_time << std::endl;
//		ofile.close();
//		// end DELETE

		//check bad features
		if(badFeatureChecker != NULL){
			printf("CheckBadFeatures...\n");
			badFeatureChecker->check(ref_features, cur_features, rMin, rMax, bad_checker_valid_features);
			for(int i=0;i<valid_features.size();i++)
				valid_features[i] = valid_features[i] & bad_checker_valid_features[i];
		}

		//construct feature to warp
		cv::Mat good_ref_features, good_cur_features;
		constructGoodFeatures(ref_features, cur_features, valid_features, good_ref_features, good_cur_features);

		// warping
		if(warper != NULL){
			printf("Warping...\n");
			warper->init(rMin, rMax, ref_frame, good_ref_features);
			warper->warp(cur_frame, good_cur_features, stabilised_frame);
		}
	}

	// add to history
	printf("Adding to history...\n");
	history->add(cur_manifold, cur_features);

	// set previous
	cur_frame.copyTo(prev_frame);
	cur_features.copyTo(prev_features);

	return true;
}

void ps::Stabiliser::addDetector(Detector *detector){
	this->detector = detector;
}

void ps::Stabiliser::addTracker(Tracker *tracker){
	this->tracker = tracker;
}

void ps::Stabiliser::addWarper(Warper *warper){
	this->warper = warper;
}

void ps::Stabiliser::addManifold(Manifold *manifold){
	this->manifold = manifold;
}

void ps::Stabiliser::addPredictor(Predictor *predictor){
	this->predictor = predictor;
}

void ps::Stabiliser::addHistory(History *history){
	this->history = history;
}

void ps::Stabiliser::addBadFeatureChecker(BadFeatureChecker *badFeatureChecker){
	this->badFeatureChecker = badFeatureChecker;
}

void ps::Stabiliser::addHighlightInpainter(HighlightInpainter *inpainter){
	this->inpainter = inpainter;
}

// for output
bool ps::Stabiliser::getCurrentFeatures(cv::Mat &features, std::vector<bool> &valid_features){
	cur_features.copyTo(features);
	valid_features.clear();
	for(int i=0;i<this->valid_features.size();i++){
		valid_features.push_back(this->valid_features[i]);
	}
	return true;
}

bool ps::Stabiliser::getStabilisedFrame(cv::Mat &frame){
	if(stabilised_frame.empty())
		return false;

	stabilised_frame.copyTo(frame);
	return true;
}

void ps::Stabiliser::constructGoodFeatures(cv::Mat &ref_features, cv::Mat &cur_features, std::vector<bool> &valid_features, cv::Mat &good_ref_features, cv::Mat &good_cur_features){
	int n_good = 0;
	for(int i=0;i<valid_features.size();i++){
		if(valid_features[i])
			n_good++;
	}

	if(n_good == 0){
		std::cerr << "No good features" << std::endl;
		ref_features.copyTo(good_ref_features);
		cur_features.copyTo(good_cur_features);
		return;
	}

	good_ref_features = cv::Mat(n_good, 2, CV_64FC1);
	good_cur_features = cv::Mat(n_good, 2, CV_64FC1);

	int cnt = 0;
	for(int i=0;i<valid_features.size();i++){
		if(valid_features[i]){
			good_ref_features.at<double>(cnt, 0) = ref_features.at<double>(i, 0);
			good_ref_features.at<double>(cnt, 1) = ref_features.at<double>(i, 1);
			good_cur_features.at<double>(cnt, 0) = cur_features.at<double>(i, 0);
			good_cur_features.at<double>(cnt, 1) = cur_features.at<double>(i, 1);
			cnt++;
		}
	}
}
