#ifndef _PS_HISTORY_H_
#define _PS_HISTORY_H_

#include<opencv2/opencv.hpp>

namespace ps{

typedef struct HistoryParam{
	int maxT;

	HistoryParam(){
		printf("Set parameters for History...\n");
		maxT = 2000;
	}
}HistoryParam;

class History{
public:
	History();

	void init(int n_features, int n_embedded_dimensions);
	void add(cv::Mat &manifold_data, cv::Mat &feature_data);
	bool getFeatureHistory(cv::Mat &X, int feature_idx, int last);

public:
	int d; //number of intrinsic dimension
	int T; // current time 0, 1, 2, ...
	cv::Mat manifold_history; // each row is each time step.
	cv::Mat feature_history; // each row is each time step.

private:
	HistoryParam param;

	void arrange();
};

}

#endif
