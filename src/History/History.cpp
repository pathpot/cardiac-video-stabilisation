#include "History.h"

ps::History::History(){
	d = 0;
	T = 0;
}

void ps::History::init(int n_features, int n_embedded_dimensions){
	d = n_embedded_dimensions;

	//initialize history storage
	manifold_history = cv::Mat(param.maxT, d, CV_64FC1); // each row is each time step.
	feature_history = cv::Mat(param.maxT, 2*n_features, CV_64FC1); // each row is each time step. (each feature needs 2 dimensions)
}

void ps::History::arrange(){
	if(T == param.maxT){
		// set T
		T = param.maxT/2;

		//copy data
		for(int t=0;t<T;t++){
			for(int i=0;i<d;i++){
				manifold_history.at<double>(t, i) = manifold_history.at<double>(T + t, i);
			}

			for(int i=0;i<feature_history.cols;i++){
				feature_history.at<double>(t, i) = feature_history.at<double>(T + t, i);
			}
		}
	}
}

void ps::History::add(cv::Mat &manifold_data, cv::Mat &feature_data){
	// check if history is full
	arrange();

	// keep in history
	for(int i=0;i<d;i++){
		manifold_history.at<double>(T, i) = manifold_data.at<double>(0, i);
	}

	for(int i=0;i<feature_history.cols/2;i++){
		for(int j=0;j<2;j++){
			feature_history.at<double>(T, 2*i + j) = feature_data.at<double>(i, j);
		}
	}

	T++;
}

bool ps::History::getFeatureHistory(cv::Mat &X, int feature_idx, int last){
	if(last < 0)
		last = T;

	if(T < last)
		return false;

	feature_history(cv::Range(T - last, T), cv::Range(2*feature_idx, 2*feature_idx + 2)).copyTo(X);
	return true;
}


