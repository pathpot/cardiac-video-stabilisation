#ifndef _PS_HIGHLIGHTINPAINTER_H_
#define _PS_HIGHLIGHTINPAINTER_H_

#include "HighlightInpainterParam.h"
#include <opencv2/opencv.hpp>
#include <queue>

namespace ps{
class HighlightInpainter{
public:
	static void segmentAndinpaint(cv::Mat &in, cv::Mat &out);

private:
	static void segmentModule1(cv::Mat &img, cv::Mat &segment, double T);
	static void segmentModule2(cv::Mat &img, cv::Mat &segment);
	static void inpaint(cv::Mat &in, cv::Mat &out, cv::Mat &segment);
	static void fill(cv::Mat &in, cv::Mat &out, cv::Mat &segment);
	static void getWeightMask(cv::Mat &segment, cv::Mat &mask);

private:
	static double dToWeight(double d);

	static HighlightInpainterParam param;

};


}


#endif
