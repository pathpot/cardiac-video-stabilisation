#ifndef _PS_HIGHLIGHTINPAINTERPARAM_H_
#define _PS_HIGHLIGHTINPAINTERPARAM_H_

#include<opencv2/opencv.hpp>

namespace ps{
class HighlightInpainterParam{
public:
	// threshold for segmentation
	double T1, T2abs, T2rel;

	// ratio of each 95th percentiles for module 1
	double rGE, rBE;

	//constrast coefficient for module 2
	double tR, tG, tB;

	// window size for median filter
	int medFilterSize;

	// dilation kernel
	int dilateMax;
	cv::Mat diskKernel2, diskKernel4;

	// gaussian kernel
	double gSigma;
	int gSize;

	// direction
	int nDir;
	int dir[4][2];

	// weight mask
	int dmax;
	double lmin, lmax, c;

	HighlightInpainterParam(){
		T1 = 225;
		T2abs = 200;
		T2rel = 2;//1.1;

		medFilterSize = 5;

		// 95th ratio
		rGE = 130.0/151;
		rBE = 135.0/151;

		// constrast coefficient
		tR = 0.6802;
		tG = 0.4969;
		tB = 0.4832;

		// direction
		nDir = 4;
		dir[0][0] = 0;
		dir[0][1] = 1;
		dir[1][0] = 0;
		dir[1][1] = -1;
		dir[2][0] = 1;
		dir[2][1] = 0;
		dir[3][0] = -1;
		dir[3][1] = 0;

		// dilation
		dilateMax = 5;

		// disk kernel with radius 2
		diskKernel2 = cv::Mat::zeros(5, 5, CV_8UC1);
		cv::circle(diskKernel2, cv::Point(5/2, 5/2), 2, 1, -1);

		diskKernel4 = cv::Mat::zeros(9, 9, CV_8UC1);
		cv::circle(diskKernel4, cv::Point(9/2, 9/2), 4, 1, -1);

		//gaussian
		gSize = 7;
		gSigma = 8.0;

		//weight mask
		dmax = 19;
		lmin = -5;
		lmax = 5;
		c = 0.7;
	}
};

}

#endif
