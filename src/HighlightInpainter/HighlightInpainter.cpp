#include "HighlightInpainter.h"

ps::HighlightInpainterParam ps::HighlightInpainter::param;

void ps::HighlightInpainter::segmentAndinpaint(cv::Mat &in, cv::Mat &out){
	if(in.type() != CV_8UC3){
		std::cerr << "The type of \"in\" must be CV_8UC3" << std::endl;
		getchar();
	}

	// 1) Segmentation

	// get segment 1
	cv::Mat segment1;
	segmentModule1(in, segment1, param.T1);

	// get segment 2
	cv::Mat segment2;
	segmentModule2(in, segment2);


	// Disjunction of two segments
	cv::Mat segment;
	cv::bitwise_or(segment1, segment2, segment);

	//cv::imshow("Segment1", segment1*255);
	//cv::imshow("Segment2", segment2*255);
	//cv::imshow("Segment", segment*255);
	//cv::imwrite("segment.png", segment(cv::Range::all(), cv::Range(segment.cols/2, segment.cols))*255);
	//cv::waitKey();

	// 2) Inpainting
	inpaint(in, out, segment);
}


// segment = 0 (nonspecular), 1(specular)
void ps::HighlightInpainter::segmentModule1(cv::Mat &img, cv::Mat &segment, double T){
	segment.release();

	segment = cv::Mat(img.rows, img.cols, CV_8UC1);
	cv::Vec3b color;
	double cG, cB, cR, cE;
	for(int i=0;i<segment.rows;i++){
		for(int j=0;j<segment.cols;j++){
			color = img.at<cv::Vec3b>(i, j);
			cB = color.val[0];
			cG = color.val[1];
			cR = color.val[2];
			cE = 0.2989*cR + 0.5870*cG + 0.1140*cB;

			//printf("%lf %lf %lf : %lf %lf %lf\n", cG, cB, cE, param.rGE*T, param.rBE*T, T);
			if(cG > param.rGE*T || cB > param.rBE*T || cE > T){
				segment.at<unsigned char>(i, j) = 1;
			}else{
				segment.at<unsigned char>(i, j) = 0;
			}
		}
	}
}


void ps::HighlightInpainter::segmentModule2(cv::Mat &img, cv::Mat &segment){
	// 1) Module1 with T2
	segmentModule1(img, segment, param.T2abs);

	// 2) Filling
	cv::Mat filledImage;
	fill(img, filledImage, segment);

	// 3) Median Filtering
	cv::Mat medImage;
	medianBlur(filledImage, medImage, param.medFilterSize);

	// 4) Compute Module2 Segment
	double eps;
	cv::Vec3b color, est_color;
	double eb, eg, er;
	for(int i=0;i<segment.rows;i++){
		for(int j=0;j<segment.cols;j++){
			color = img.at<cv::Vec3b>(i, j);
			est_color = medImage.at<cv::Vec3b>(i, j);

			if(est_color.val[0] == 0 || est_color.val[1] == 0 || est_color.val[2] == 0){
				eps = 0;
			}else{
				eb = param.tB*color.val[0]/est_color.val[0];
				eg = param.tG*color.val[1]/est_color.val[1];
				er = param.tR*color.val[2]/est_color.val[2];

				eps = eb;
				if(eps < eg) eps = eg;
				if(eps < er) eps = er;
			}

			if(eps > param.T2rel){
				segment.at<unsigned char>(i, j) = 1;
			}else{
				segment.at<unsigned char>(i, j) = 0;
			}
		}
	}

	//cv::imshow("Image", img);
	//cv::imshow("MedImage", medImage);
	//cv::imshow("Segment2", segment*255);
	//cv::waitKey();
}

void ps::HighlightInpainter::fill(cv::Mat &in, cv::Mat &out, cv::Mat &segment){
	in.copyTo(out);

	cv::Mat temp_segment;
	cv::Mat regionImg, dil2, dil4, dil_xor;
	segment.copyTo(temp_segment);

	std::vector<std::pair<int, int> > regionVect, searchQ;
	std::pair<int, int> curPix, nextPix;
	double meanR, meanG, meanB;
	cv::Vec3b temp_color;
	cv::Point2i minP, maxP;
	int Nsp;
	regionImg = cv::Mat::zeros(temp_segment.rows, temp_segment.cols, CV_8UC1);
	dil_xor =  cv::Mat::zeros(temp_segment.rows, temp_segment.cols, CV_8UC1);

	for(int pi=0;pi<temp_segment.rows;pi++){
		for(int pj=0;pj<temp_segment.cols;pj++){
			if(temp_segment.at<unsigned char>(pi, pj) == 0) //nonspecular
				continue;

			// 0) get all pixel in the region by DFS
			searchQ.clear();
			regionVect.clear();
			searchQ.push_back(std::make_pair(pi, pj));
			regionVect.push_back(std::make_pair(pi, pj));

			minP.y = maxP.y = pi;
			minP.x = maxP.x = pj;

			while(searchQ.size() != 0){
				// get last element
				curPix = searchQ[searchQ.size() - 1];
				searchQ.pop_back();

				// set min max
				minP.y = (curPix.first < minP.y) ? curPix.first : minP.y;
				maxP.y = (curPix.first > maxP.y) ? curPix.first : maxP.y;
				minP.x = (curPix.second < minP.x) ? curPix.second : minP.x;
				maxP.x = (curPix.second > maxP.x) ? curPix.second : maxP.x;

				for(int d=0;d<param.nDir;d++){
					nextPix.first = curPix.first + param.dir[d][0];
					nextPix.second = curPix.second + param.dir[d][1];

					// out of bound
					if(nextPix.first < 0 || nextPix.first >= temp_segment.rows || nextPix.second < 0 || nextPix.second >= temp_segment.cols)
						continue;

					// nonspecular or observed
					if(temp_segment.at<unsigned char>(nextPix.first, nextPix.second) == 0)
						continue;

					// observe
					temp_segment.at<unsigned char>(nextPix.first, nextPix.second) = 0;
					searchQ.push_back(nextPix);
					regionVect.push_back(nextPix);
				}
			}

			// 1) Set Region
			regionImg.release();
			dil2.release();
			dil4.release();
			dil_xor.release();
			regionImg = cv::Mat::zeros((maxP.y + param.dilateMax) - (minP.y - param.dilateMax), (maxP.x + param.dilateMax) - (minP.x - param.dilateMax), CV_8UC1);
			for(int k=0;k<regionVect.size();k++){
				regionImg.at<unsigned char>(regionVect[k].first - (minP.y - param.dilateMax), regionVect[k].second - (minP.x - param.dilateMax)) = 1;
			}

			// 2) 2 times dilation
			cv::dilate(regionImg, dil2, param.diskKernel2);
			cv::dilate(regionImg, dil4, param.diskKernel4);

			// 3) exclusive disjunction area
			cv::bitwise_xor(dil2, dil4, dil_xor);

			// 4) get mean color
			meanR = meanG = meanB = 0;
			Nsp = 0;
			for(int di=0;di<dil_xor.rows;di++){
				for(int dj=0;dj<dil_xor.cols;dj++){
					if(dil_xor.at<unsigned char>(di, dj) > 0){
						int cx, cy;
						cy = di + (minP.y - param.dilateMax);
						cx = dj + (minP.x - param.dilateMax);

						if(cx < 0 || cy < 0 || cx >= in.cols || cy >= in.rows)
							continue;

						temp_color = in.at<cv::Vec3b>(cy, cx);

						meanB += temp_color.val[0];
						meanG += temp_color.val[1];
						meanR += temp_color.val[2];
						Nsp++;
					}
				}
			}
			meanB /= Nsp;
			meanG /= Nsp;
			meanR /= Nsp;
			temp_color.val[0] = (int)meanB;
			temp_color.val[1] = (int)meanG;
			temp_color.val[2] = (int)meanR;

			// 5) set to all in the region
			for(int k=regionVect.size()-1;k>=0;k--){
				out.at<cv::Vec3b>(regionVect[k].first, regionVect[k].second) = temp_color;
			}
		}
	}
}

void ps::HighlightInpainter::inpaint(cv::Mat &in, cv::Mat &out, cv::Mat &segment){
	// 2.1) Gaussian Filter to filled Image
	cv::Mat filledImg, gaussImg;
	fill(in, filledImg, segment);
	cv::GaussianBlur(filledImg, gaussImg, cv::Size(param.gSize, param.gSize), param.gSigma);
	// cv::imshow("G", gaussImg);
	// cv::waitKey();

	// 2.2) Compute Weight Mask
	cv::Mat mask;
	getWeightMask(segment, mask);

	// 2.3) Produce Inpainted Image
	in.copyTo(out);
	cv::Vec3b csm, c;
	int newr, newg, newb;
	double m;
	for(int i=0;i<out.rows;i++){
		for(int j=0;j<out.cols;j++){
			csm = gaussImg.at<cv::Vec3b>(i, j);
			c = in.at<cv::Vec3b>(i, j);
			m = mask.at<double>(i, j);

			newb = (int)round(m*csm.val[0] + (1-m)*c.val[0]);
			newg = (int)round(m*csm.val[1] + (1-m)*c.val[1]);
			newr = (int)round(m*csm.val[2] + (1-m)*c.val[2]);

			out.at<cv::Vec3b>(i, j) = cv::Vec3b(newb, newg, newr);
		}
	}

	//cv::imshow("Out", out); cv::waitKey();
}

void ps::HighlightInpainter::getWeightMask(cv::Mat &segment, cv::Mat &mask){
	//printf("Begin : getWeightMask\n");
	int nrows = segment.rows;
	int ncols = segment.cols;
	cv::Mat mask_d = cv::Mat::ones(nrows, ncols, CV_8UC1)*(param.dmax + 1);
	std::queue<std::pair<int, int> > queue;
	for(int i= 0; i<nrows;i++){
		for(int j = 0;j<ncols;j++){
			if(segment.at<unsigned char>(i , j) > 0){
				queue.push(std::make_pair(i, j));
				mask_d.at<unsigned char>(i, j) = 0;
			}
		}
	}

	std::pair<int, int> curPix, nextPix;
	double curD, nextD;
	while(queue.size() != 0){
		curPix = queue.front();
		queue.pop();
		curD = mask_d.at<unsigned char>(curPix.first, curPix.second);

		nextD = curD + 1;

		if(nextD > param.dmax)
			continue;

		for(int d=0;d<param.nDir;d++){
			nextPix.first = curPix.first + param.dir[d][0];
			nextPix.second = curPix.second + param.dir[d][1];

			// out of bound
			if(nextPix.first < 0 || nextPix.first >= nrows || nextPix.second < 0 || nextPix.second >= ncols)
				continue;

			if(mask_d.at<unsigned char>(nextPix.first, nextPix.second) > nextD){
				mask_d.at<unsigned char>(nextPix.first, nextPix.second) = nextD;
				queue.push(nextPix);
			}
		}
	}

	//change mask_d to mask
	mask.release();
	mask = cv::Mat::zeros(nrows, ncols, CV_64FC1);
	double myD;
	for(int i=0;i<nrows;i++){
		for(int j=0;j<ncols;j++){
			myD = (double)mask_d.at<unsigned char>(i, j);
			if(myD <= param.dmax){
				mask.at<double>(i, j) = dToWeight(myD);
			}
		}
	}
	//printf("End : getWeightMask\n");
}

double ps::HighlightInpainter::dToWeight(double d){
	double exponent = (param.lmax - param.lmin)*pow(d/param.dmax, param.c) + param.lmin;
	return 1/(1 + exp(exponent));
}

//void ps::SpecularInpainter::fill(cv::Mat &in, cv::Mat &out, cv::Mat &segment){
//	printf("Begin : Fill\n");
//	in.copyTo(out);
//
//	cv::Mat temp_segment;
//	cv::Mat regionImg, dil2, dil4, dil_xor;
//	segment.copyTo(temp_segment);
//
//	std::vector<std::pair<int, int> > regionVect, searchQ;
//	std::pair<int, int> curPix, nextPix;
//	double meanR, meanG, meanB;
//	cv::Vec3b temp_color;
//	int Nsp;
//	regionImg = cv::Mat::zeros(temp_segment.rows, temp_segment.cols, CV_8UC1);
//	dil_xor =  cv::Mat::zeros(temp_segment.rows, temp_segment.cols, CV_8UC1);
//
//	for(int pi=0;pi<temp_segment.rows;pi++){
//		for(int pj=0;pj<temp_segment.cols;pj++){
//			if(temp_segment.at<unsigned char>(pi, pj) == 0) //nonspecular
//				continue;
//
//			// 0) get all pixel in the region by DFS
//			searchQ.clear();
//			regionVect.clear();
//			searchQ.push_back(std::make_pair(pi, pj));
//			regionVect.push_back(std::make_pair(pi, pj));
//
//			while(searchQ.size() != 0){
//				// get last element
//				curPix = searchQ[searchQ.size() - 1];
//				searchQ.pop_back();
//
//				for(int d=0;d<param.nDir;d++){
//					nextPix.first = curPix.first + param.dir[d][0];
//					nextPix.second = curPix.second + param.dir[d][1];
//
//					// out of bound
//					if(nextPix.first < 0 || nextPix.first >= temp_segment.rows || nextPix.second < 0 || nextPix.second >= temp_segment.cols)
//						continue;
//
//					// nonspecular or observed
//					if(temp_segment.at<unsigned char>(nextPix.first, nextPix.second) == 0)
//						continue;
//
//					// observe
//					temp_segment.at<unsigned char>(nextPix.first, nextPix.second) = 0;
//					searchQ.push_back(nextPix);
//					regionVect.push_back(nextPix);
//				}
//			}
//
//			// 1) Set Region
//			regionImg.release();
//			regionImg = cv::Mat::zeros(temp_segment.rows, temp_segment.cols, CV_8UC1);
//			for(int k=0;k<regionVect.size();k++){
//				regionImg.at<unsigned char>(regionVect[k].first, regionVect[k].second) = 1;
//			}
//
//			// 2) 2 times dilation
//			cv::dilate(regionImg, dil2, param.diskKernel2);
//			cv::dilate(regionImg, dil4, param.diskKernel4);
//
//			// 3) exclusive disjunction area
//			cv::bitwise_xor(dil2, dil4, dil_xor);
//
//			// 4) get mean color
//			meanR = meanG = meanB = 0;
//			Nsp = 0;
//			for(int di=0;di<dil_xor.rows;di++){
//				for(int dj=0;dj<dil_xor.cols;dj++){
//					if(dil_xor.at<unsigned char>(di, dj) > 0){
//						temp_color = in.at<cv::Vec3b>(di, dj);
//						meanB += temp_color.val[0];
//						meanG += temp_color.val[1];
//						meanR += temp_color.val[2];
//						Nsp++;
//					}
//				}
//			}
//			meanB /= Nsp;
//			meanG /= Nsp;
//			meanR /= Nsp;
//			temp_color.val[0] = (int)meanB;
//			temp_color.val[1] = (int)meanG;
//			temp_color.val[2] = (int)meanR;
//
//			// 5) set to all in the region
//			for(int k=regionVect.size()-1;k>=0;k--){
//				out.at<cv::Vec3b>(regionVect[k].first, regionVect[k].second) = temp_color;
//			}
//
//		}
//	}
//
//	printf("End : Fill\n");
//}

