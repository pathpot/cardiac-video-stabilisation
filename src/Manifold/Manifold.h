#ifndef _PS_MANIFOLD_H_
#define _PS_MANIFOLD_H_

#include<opencv2/opencv.hpp>

namespace ps{
class Manifold{
public:
	virtual ~Manifold(){};

	// project (all column vector)
	virtual void projectImage(cv::Mat &_observation, cv::Mat &manifold) = 0;
	virtual void cvtImagetoVector(cv::Mat &_image, cv::Mat &observation) = 0;
	virtual int getIntrinsicDim() = 0;
};
}


#endif
