#ifndef _PS_LINEARMANIFOLD_H_
#define _PS_LINEARMANIFOLD_H_

#include <opencv2/opencv.hpp>
#include "Manifold.h"

namespace ps{

class LinearManifold : public Manifold{

public:
	LinearManifold(std::string _manifold_filename);

	// project (all column vector)
	void projectImage(cv::Mat &_observation, cv::Mat &manifold);
	void cvtImagetoVector(cv::Mat &_image, cv::Mat &vector);
	int getIntrinsicDim();

private:
	int start_x, start_y, end_x, end_y;
	// projection matrix Y = (X - mean)*W -- each data is in each row of X
	cv::Mat M, mean;
};


}


#endif
