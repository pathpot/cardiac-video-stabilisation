#include "LinearManifold.h"

ps::LinearManifold::LinearManifold(std::string _manifold_filename){
	//open file
	cv::FileStorage fs(_manifold_filename, cv::FileStorage::READ);
	if(!fs.isOpened()){
		std::cerr << "Cannot open file " << _manifold_filename << std::endl;
		getchar();
	}

	// read data
	fs["mean"] >> mean;
	fs["M"] >> M;

	cv::Mat startPix, endPix;
	fs["startPix"] >> startPix;
	fs["endPix"] >> endPix;

	start_x = (int)round(startPix.at<double>(0, 0));
	start_y = (int)round(startPix.at<double>(0, 1));
	end_x = (int)round(endPix.at<double>(0, 0));
	end_y = (int)round(endPix.at<double>(0, 1));

	startPix.release();
	endPix.release();

	// close file
	fs.release();
}

void ps::LinearManifold::projectImage(cv::Mat &_observation, cv::Mat &manifold){
	cv::Mat vector;
	cvtImagetoVector(_observation, vector);
	manifold = (vector - mean)*M;
}

int ps::LinearManifold::getIntrinsicDim(){
	return M.cols;
}

void ps::LinearManifold::cvtImagetoVector(cv::Mat &_image, cv::Mat &vector){
	vector.release();
	vector = cv::Mat(1, (end_y - start_y)*(end_x - start_x), CV_64FC1);

	int cnt = 0;
	for(int i=start_y;i<end_y;i++){
		for(int j=start_x;j<end_x;j++){
			vector.at<double>(0, cnt++) = (double)_image.at<unsigned char>(i, j);
		}
	}
}
