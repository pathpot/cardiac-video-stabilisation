//============================================================================
// Name        : CardiacVideoStabilisation.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <fstream>
#include <string>
#include "Stabiliser/Stabiliser.h"
#include "Detector/FASTDetector.h"
#include "Detector/ManualDetector.h"
#include "Tracker/HybridTracker.h"
#include "Warper/TPSWarper.h"
#include "Warper/AffineWarper.h"
#include "Manifold/LinearManifold.h"
#include "Predictor/EVPredictor.h"
#include "History/History.h"
#include <opencv2/opencv.hpp>


void drawFeatures(cv::Mat &frame, cv::Mat &features, std::vector<bool> &valid_features){
	cv::Point2d control_point;
	cv::Scalar colour;
	for(int i=0;i<features.rows;i++){
		if(valid_features[i]){
			colour = cv::Scalar(0, 255, 0);
		}else{
			colour = cv::Scalar(0, 0, 255);
		}

		control_point = cv::Point2d(features.at<double>(i, 0), features.at<double>(i, 1));
		cv::circle(frame, control_point, 2, colour, -1);
	}

}

int main() {
	bool useInpainter = true;
	bool useManualDetector = false;
	bool checkBadFeature = true;
	std::string detected_feature_filename = "../data/gen_seq_init_features.txt";

	// define input
	// patch A
//	std::string video_filename = "../data/hamlyn_all.avi";
//	std::string manifold_filename = "param/mapping_patchA.yml";
//	cv::Point2d minCorner(558, 91), maxCorner(595, 130);


//	// patch B
//	std::string video_filename = "../data/hamlyn_all.avi";
//	std::string manifold_filename = "param/mapping_patchB.yml";
//	cv::Point2d minCorner(572, 198), maxCorner(611, 260);

	// genseq 2
	std::string video_filename = "../data/gen_seq2.avi";
	std::string manifold_filename = "param/mapping_gen_seq2.yml";
	cv::Point2d minCorner(0, 0), maxCorner(99, 99);
	//useManualDetector = true;
	useInpainter = false;
	checkBadFeature = false;

//	 //genseq 3
//	std::string video_filename = "../data/gen_seq3.avi";
//	std::string manifold_filename = "param/mapping_gen_seq3.yml";
//	cv::Point2d minCorner(0, 0), maxCorner(99, 99);
//	//useManualDetector = true;
//	useInpainter = false;
//	checkBadFeature = false;



	// define stabiliser
	ps::ManualDetector manual_detector(detected_feature_filename);
	ps::FASTDetector fast_detector;
	ps::TPSWarper warper; //ps::AffineWarper warper;
	ps::EVPredictor predictor;
	ps::History history;


	ps::HybridTracker tracker(&history); //ps::RTracker tracker;
	ps::LinearManifold manifold(manifold_filename);
	ps::BadFeatureChecker badChecker;
	ps::HighlightInpainter inpainter;

	ps::Stabiliser stabiliser(minCorner, maxCorner);
	stabiliser.addTracker(&tracker);
	stabiliser.addWarper(&warper);
	stabiliser.addManifold(&manifold);
	stabiliser.addPredictor(&predictor);
	stabiliser.addHistory(&history);


	if(checkBadFeature)
		stabiliser.addBadFeatureChecker(&badChecker);

	if(useManualDetector)
		stabiliser.addDetector(&manual_detector);
	else
		stabiliser.addDetector(&fast_detector);

	if(useInpainter)
		stabiliser.addHighlightInpainter(&inpainter);

	//open video
	cv::VideoCapture capture = cv::VideoCapture(video_filename);
	if(!capture.isOpened()){
			std::cerr << "Cannot open video " << video_filename << std::endl;
	        return false;
	};

	int n_frame = 0;
	cv::Mat frame, cur_features, stabilised_frame;
	std::vector<bool> valid_features;
	while(true){
		//read frame
		capture >> frame;
		if(frame.empty()){
			std::cerr << "End of the video!" << std::endl;
			break;
		}

		printf("Frame : %d\n", ++n_frame);

		//update stabiliser
		stabiliser.process(frame);

		//get output from the stabiliser
		if(stabiliser.getCurrentFeatures(cur_features, valid_features)){
			drawFeatures(frame, cur_features, valid_features);
		}

		//write some output to files
//		std::ofstream ofile;
//		ofile.open("RTracker_features.txt", std::ios::out | std::ios::app);
//		ofile << cur_features.at<double>(4, 0) << " " << cur_features.at<double>(4, 1) << std::endl;
//		ofile.close();

		//get stabilised frame
		if(stabiliser.getStabilisedFrame(stabilised_frame)){
			cv::imshow("Stabilised Frame", stabilised_frame);
		}

		//TODO save frame
		cv::imwrite("img" + std::to_string(n_frame) + ".png", stabilised_frame);

		//show frame
		cv::imshow("Frame", frame);
		if(cv::waitKey(50) == 27){
			printf("Abort by user.\n");
			break;
		}
	}

	return 0;
}

// Detector
//	-FAST/

// Tracker
//	- P-Tracker/
//	- R-Tracker/
//	- predictive R-Tracker with LPP/
//	- predictive R-Tracker with TLE
//	- Hybrid Tracker with NCC
//	- Hybrid Tracker with MCD

// Sub Tracker
//	- Optical Flow/
// 	- ESM
// 	- Dense Flow

// Warper
//	- Affine
//	- TPS/
//	Context

// Remover
//	- Affine Remover
//  - Mistrack Remover

// Remove feature
// Remove predictor


// affine + remove + hybrid
