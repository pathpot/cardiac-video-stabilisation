#ifndef _PS_AFFINEWARPER_H_
#define _PS_AFFINEWARPER_H_

#include "Warper.h"
#include "../Tools/Affine/Affine.h"

namespace ps{
class AffineWarper : public Warper{
public:
	virtual void init(cv::Point2d &_minCorner, cv::Point2d &_maxCorner, cv::Mat &_ref_frame, cv::Mat &_ref_features);
	virtual void warp(cv::Mat &_cur_frame, cv::Mat &_cur_features, cv::Mat &stabilised_frame);

private:
	cv::Point2d ref_MinCorner, ref_MaxCorner;
	cv::Mat ref_features;
};

}

#endif
