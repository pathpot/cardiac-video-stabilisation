#ifndef _PS_WARPER_H_
#define _PS_WARPER_H_

#include <opencv2/opencv.hpp>

namespace ps{
class Warper{
public:
	virtual ~Warper(){};
	virtual void init(cv::Point2d &_minCorner, cv::Point2d &_maxCorner, cv::Mat &_ref_frame, cv::Mat &_ref_features) = 0;
	virtual void warp(cv::Mat &_cur_frame, cv::Mat &_cur_features, cv::Mat &stabilised_patch) = 0;
};
}


#endif
