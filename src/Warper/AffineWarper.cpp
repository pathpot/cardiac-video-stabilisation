#include "AffineWarper.h"

void ps::AffineWarper::init(cv::Point2d &_minCorner, cv::Point2d &_maxCorner, cv::Mat &_ref_frame, cv::Mat &_ref_features){
	ref_MinCorner = _minCorner;
	ref_MaxCorner = _maxCorner;
	_ref_features.copyTo(this->ref_features);
}

void ps::AffineWarper::warp(cv::Mat &_cur_frame, cv::Mat &_cur_features, cv::Mat &stabilised_patch){
	cv::Mat affineCtoR;
	Affine::estimateAffineLS(ref_features, _cur_features, affineCtoR);
	Affine::getStabilizedPatch(_cur_frame, stabilised_patch, affineCtoR, ref_MinCorner, ref_MaxCorner);
}
