#include "TPSWarper.h"

void ps::TPSWarper::init(cv::Point2d &_minCorner, cv::Point2d &_maxCorner, cv::Mat &_ref_frame, cv::Mat &_ref_features){
	if(_ref_frame.type() != CV_8UC1){
		std::cerr << "The type of _ref_frame must be CV_8UC1" << std::endl;
		getchar();
	}

	ref_MinCorner = _minCorner;
	ref_MaxCorner = _maxCorner;

	// Set vector of all pixel in the boundary
	// (set reference pixels and project pixels)
	ref_pixels.release();
	int N = ((int)round(_maxCorner.y - _minCorner.y + 1))*((int)round(_maxCorner.x - _minCorner.x + 1)); // set number of patch pixels
	ref_pixels = cv::Mat(N, 2, CV_64FC1);

	int cnt = 0;
	for(int y = _minCorner.y; y<=_maxCorner.y; y++){
		for(int x = _minCorner.x; x<=_maxCorner.x; x++){
			ref_pixels.at<double>(cnt, 0) = x;
			ref_pixels.at<double>(cnt, 1) = y;
			cnt++;
		}
	}

	// Calculate K*
	TPS::constructKS(_ref_features, KS);

	// Calculate M
	TPS::constructM(ref_pixels, _ref_features, M);
}

void ps::TPSWarper::warp(cv::Mat &_cur_frame, cv::Mat &_cur_features, cv::Mat &stabilised_frame){
	TPS::getStabilizedPatch(_cur_frame, stabilised_frame, ref_pixels, _cur_features, ref_MinCorner, ref_MaxCorner, M, KS);
}
