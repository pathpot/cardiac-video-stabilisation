#ifndef _PS_BADFEATURECHECKER_H_
#define _PS_BADFEATURECHECKER_H_

#include <opencv2/opencv.hpp>
#include "../Tools/Affine/Affine.h"

namespace ps{
class BadFeatureChecker{
public:
	void check(cv::Mat &_ref_features, cv::Mat &_cur_features, cv::Point2d &ref_MinCorner, cv::Point2d &ref_MaxCorner, std::vector<bool> &valid);

private:
	bool isInsideRect(cv::Point2d x, cv::Point2d p0,  cv::Point2d p1,  cv::Point2d p2,  cv::Point2d p3);
	bool isLeft(cv::Point2d x, cv::Point2d p0, cv::Point2d p1);
};

}

#endif
