#include "BadFeatureChecker.h"

void ps::BadFeatureChecker::check(cv::Mat &_ref_features, cv::Mat &_cur_features, cv::Point2d &ref_MinCorner, cv::Point2d &ref_MaxCorner, std::vector<bool> &valid){
	//estimate affine
	cv::Mat affineRtoC;
	bool good = Affine::estimateAffine(_ref_features, _cur_features, affineRtoC);

	// project boundary
	cv::Point2d pMin = Affine::transform(ref_MinCorner, affineRtoC);
	cv::Point2d pMax = Affine::transform(ref_MaxCorner, affineRtoC);
	cv::Point2d pMinMax = Affine::transform(cv::Point2d(ref_MinCorner.x, ref_MaxCorner.y), affineRtoC);
	cv::Point2d pMaxMin = Affine::transform(cv::Point2d(ref_MaxCorner.x, ref_MinCorner.y), affineRtoC);

	double px, py;
	valid.clear();
	for(int i=0;i<_cur_features.rows;i++){
		px = _cur_features.at<double>(i, 0);
		py = _cur_features.at<double>(i, 1);
		valid.push_back(!good || isInsideRect(cv::Point2d(px, py), pMin, pMinMax, pMax, pMaxMin));
	}

}

bool ps::BadFeatureChecker::isInsideRect(cv::Point2d x, cv::Point2d p0,  cv::Point2d p1,  cv::Point2d p2,  cv::Point2d p3){
	bool l0 = isLeft(x, p0, p1);
	bool l1 = isLeft(x, p1, p2);
	bool l2 = isLeft(x, p2, p3);
	bool l3 = isLeft(x, p3, p0);

	return (l0 && l1 && l2 && l3) || (!l0 && !l1 && !l2 && !l3);
}

bool ps::BadFeatureChecker::isLeft(cv::Point2d x, cv::Point2d p0, cv::Point2d p1){
	cv::Point2d v0 = cv::Point2d(p1.x - p0.x, p1.y - p0.y);
	cv::Point2d v1 = cv::Point2d(x.x - p1.x, x.y - p1.y);


	return v0.x*v1.y - v0.y*v1.x > 0;
}


