#ifndef _PS_FASTDETECTOR_H_
#define _PS_FASTDETECTOR_H_

#include "Detector.h"

namespace ps{

// Parameter for the class
typedef struct FASTDetectorParam{
	int fastThreshold;
	FASTDetectorParam(){
		printf("Set parameters for FASTDetector...\n");
		//TODO set FAST detector
		fastThreshold = 15;
		//fastThreshold = 40; //for lady image
	}
}FASTDetectorParam;

// Class for FAST feature detection
class FASTDetector : public Detector{
public:
	virtual bool detect(cv::Mat &_frame, cv::Mat &features, cv::Point2d minCorner, cv::Point2d maxCorner);

protected:
	FASTDetectorParam param;
};

}


#endif
