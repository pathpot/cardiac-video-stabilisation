#include "ManualDetector.h"

ps::ManualDetector::ManualDetector(std::string feature_filename){
	FILE *file = NULL;
	file = fopen(feature_filename.c_str(), "r");
	if(file == NULL){
		std::cerr << "Cannot open file " << feature_filename << std::endl;
	}

	int n;
	double px, py;
	fscanf(file, "%d", &n);
	manual_features = cv::Mat(n, 2, CV_64FC1);
	for(int i=0;i<n;i++){
		fscanf(file, "%lf %lf", &px, &py);
		manual_features.at<double>(i, 0) = px;
		manual_features.at<double>(i, 1) = py;
	}
	// end
	fclose(file);
}

bool ps::ManualDetector::detect(cv::Mat &_frame, cv::Mat &features, cv::Point2d minCorner, cv::Point2d maxCorner){
	if(_frame.type() != CV_8UC1){
		std::cerr << "The type of _frame must be CV_8UC1" << std::endl;
		return false;
	}

	features.release();
	manual_features.copyTo(features);
	return true;
}
