#include "FASTDetector.h"

bool ps::FASTDetector::detect(cv::Mat &_frame, cv::Mat &features, cv::Point2d minCorner, cv::Point2d maxCorner){
	if(_frame.type() != CV_8UC1){
		std::cerr << "The type of _frame must be CV_8UC1" << std::endl;
		return false;
	}

	std::vector<cv::KeyPoint> features_vect;
	cv::FastFeatureDetector detector = cv::FastFeatureDetector(param.fastThreshold, true);
	detector.detect(_frame, features_vect);

	// remove point outside the boundary
	for(int i=features_vect.size() - 1;i>=0;i--){
		if(((cv::KeyPoint)(features_vect[i])).pt.x < minCorner.x || ((cv::KeyPoint)(features_vect[i])).pt.x > maxCorner.x || ((cv::KeyPoint)(features_vect[i])).pt.y < minCorner.y || ((cv::KeyPoint)(features_vect[i])).pt.y > maxCorner.y)
			features_vect.erase(features_vect.begin() + i);
	}

	features.release();
	features = cv::Mat(features_vect.size(), 2, CV_64FC1);

	for(int i=0;i<features_vect.size();i++){
		features.at<double>(i, 0) = ((cv::KeyPoint)(features_vect[i])).pt.x;
		features.at<double>(i, 1) = ((cv::KeyPoint)(features_vect[i])).pt.y;
	}

	return true;
}
