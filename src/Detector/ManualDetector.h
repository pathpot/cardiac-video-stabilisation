#ifndef _PS_MANUALDETECTOR_H_
#define _PS_MANUALDETECTOR_H_

#include "Detector.h"

namespace ps{
class ManualDetector : public Detector{
public:
	ManualDetector(std::string feature_filename);
	virtual bool detect(cv::Mat &_frame, cv::Mat &features, cv::Point2d minCorner, cv::Point2d maxCorner);

private:
	cv::Mat manual_features;
};
}

#endif
