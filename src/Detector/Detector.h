#ifndef _PS_DETECTOR_H_
#define _PS_DETECTOR_H_

#include <opencv2/opencv.hpp>

namespace ps{
class Detector{
public:
	virtual ~Detector(){};
	virtual bool detect(cv::Mat &_frame, cv::Mat &features, cv::Point2d minCorner, cv::Point2d maxCorner) = 0;
};
}


#endif
