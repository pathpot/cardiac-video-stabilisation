#ifndef _PS_EVPREDICTOR_H_
#define _PS_EVPREDICTOR_H_

#include "Predictor.h"

namespace ps{
typedef struct EVPredictorParam{
	int p, h, M, trainFrames;

	EVPredictorParam(){
		printf("Set parameters for EVPredictor...\n");
		p = 5; //lenght of embedded vector
		h = 2;
		M = 5;
		//TODO set predictor's train frame
		trainFrames = 200;

	}

}EVPredictorParam;


class EVPredictor : public Predictor{
public:
	EVPredictor();

	// predict feature position
	virtual bool predictf(cv::Mat &_cur_manifold, cv::Mat &_feature_history, cv::Mat &_manifold_history, int T_history, cv::Mat &features);

private:
	double SinglePredict(cv::Mat &_cur_manifold, cv::Mat &_feature_history, cv::Mat &_manifold_history, int T_history, int feature_idx);
	double EmVectDistance(cv::Mat &_cur_manifold, cv::Mat &_manifold_history, int T_history,int k1, int k2);
	double EmVect(cv::Mat &_cur_manifold, cv::Mat &_manifold_history, int T_history, int k, int i, int manifold_dim);

	EVPredictorParam param;

	//temp
	std::pair<double, int> *delta_and_I;
};

}

#endif

