#include "EVPredictor.h"

ps::EVPredictor::EVPredictor(){
	delta_and_I = NULL;
}

bool ps::EVPredictor::predictf(cv::Mat &_cur_manifold, cv::Mat &_feature_history, cv::Mat &_manifold_history, int T_history, cv::Mat &features){
	if(T_history < param.trainFrames)
		return false;

	delta_and_I = new std::pair<double, int>[T_history];

	// number of features
	int n = _feature_history.cols/2;

	// predict
	int feature_idx;
	for(int i=0;i<n;i++){
		for(int j=0;j<2;j++){
			feature_idx = 2*i + j;
			features.at<double>(i, j) = SinglePredict(_cur_manifold, _feature_history, _manifold_history, T_history, feature_idx);
		}
	}



	return true;


	delete[] delta_and_I;
	return true;
}

double ps::EVPredictor::EmVect(cv::Mat &_cur_manifold, cv::Mat &_manifold_history, int T_history, int k, int i, int manifold_dim){
	if(k - i*param.h == T_history)
		return _cur_manifold.at<double>(0, manifold_dim);
	return _manifold_history.at<double>(k - i*param.h, manifold_dim);
}

double ps::EVPredictor::EmVectDistance(cv::Mat &_cur_manifold, cv::Mat &_manifold_history, int T_history,int k1, int k2){
    double sum = 0;
    double diff;
    for(int manifold_dim = 0; manifold_dim < _manifold_history.cols; manifold_dim++){
		for(int i=0;i<param.p;i++){
			diff = EmVect(_cur_manifold, _manifold_history, T_history, k1, i, manifold_dim) - EmVect(_cur_manifold, _manifold_history, T_history, k2, i, manifold_dim);
			sum = sum + diff*diff;
		}
    }

    return sqrt(sum);
}

double ps::EVPredictor::SinglePredict(cv::Mat &_cur_manifold, cv::Mat &_feature_history, cv::Mat &_manifold_history, int T_history, int feature_idx){
    //int k = T_history-1;
	int k = T_history;

    //1. Find M most similar embedding vectors in the past together with their distances into account
    int min_I = 1;
    int max_I = (k-2-(param.p-1)*param.h);
    int In = max_I - min_I + 1;

    for(int i=0;i<In;i++){
    	delta_and_I[i].second = min_I + i;
    }

    // (calculate distance for each past embedding vector)
    for(int i=0;i<In;i++){
    	delta_and_I[i].first = EmVectDistance(_cur_manifold, _manifold_history, T_history, k, k - delta_and_I[i].second);
    }

    // (sort) - (use only first M)
    std::sort(delta_and_I, delta_and_I + In);

    // 3. Predict 1-step ahead
    double N = 0;
    for(int i=0;i<param.M;i++){
    	N += 1/delta_and_I[i].first;
    }

    double xOut = 0;
    double curPred;
    for(int i=0;i<param.M;i++){
    	curPred = _feature_history.at<double>(k - delta_and_I[i].second, feature_idx);
        if(abs(delta_and_I[i].first) < DBL_EPSILON){
            xOut = curPred;
            break;
        }

        double w = 1/(delta_and_I[i].first*N);
        xOut = xOut + w*curPred;
    }

    return xOut;
}

//double ps::EVPredictor::SinglePredict(cv::Mat &_cur_manifold, cv::Mat &_feature_history, cv::Mat &_manifold_history, int T_history, int feature_idx){
//    int k = T_history-1;
//
//    //1. Find M most similar embedding vectors in the past together with their distances into account
//    int min_I = 1;
//    int max_I = (k-2-(param.p-1)*param.h);
//    int In = max_I - min_I + 1;
//
//    for(int i=0;i<In;i++){
//    	delta_and_I[i].second = min_I + i;
//    }
//
//    // (calculate distance for each past embedding vector)
//    for(int i=0;i<In;i++){
//    	delta_and_I[i].first = EmVectDistance(_manifold_history, k, k - delta_and_I[i].second);
//    }
//
//    // (sort) - (use only first M)
//    std::sort(delta_and_I, delta_and_I + In);
//
//    // 3. Predict 1-step ahead
//    double N = 0;
//    for(int i=0;i<param.M;i++){
//    	N += 1/delta_and_I[i].first;
//    }
//
//    double xOut = 0;
//    double curPred;
//    for(int i=0;i<param.M;i++){
//    	curPred = _feature_history.at<double>(k - delta_and_I[i].second + 1, feature_idx);
//        if(abs(delta_and_I[i].first) < DBL_EPSILON){
//            xOut = curPred;
//            break;
//        }
//
//        double w = 1/(delta_and_I[i].first*N);
//        xOut = xOut + w*curPred;
//    }
//
//    return xOut;
//}
