#ifndef _PS_PREDICTOR_H_
#define _PS_PREDICTOR_H_

#include <opencv2/opencv.hpp>

namespace ps{

class Predictor{
public:
	virtual ~Predictor(){};

	virtual bool predictf(cv::Mat &_cur_manifold, cv::Mat &_feature_history, cv::Mat &_manifold_history, int T_history, cv::Mat &features) = 0;

};
}



#endif
