#ifndef _PS_HybridTracker_H_
#define _PS_HybridTracker_H_

#include "fstream"
#include "RTracker.h"
#include "../Tools/OpticalFlow/OpticalFlow.h"
#include "../Tools/PatchMeasure/PatchMeasure.h"
#include "../Tools/RobustStat/RobustStat.h"
#include "../History/History.h"

namespace ps{

typedef struct HybridTrackerParam{
	int mcdTrainFrame;
	int mcdInitN, mcdSelectN;
	double robustDistanceOutlierThreshold;
	HybridTrackerParam(){
		printf("Set parameters for HybridTracker...\n");
		//TODO set hybrid tracker train frame
		mcdTrainFrame = 200;
		mcdInitN = 50;
		mcdSelectN = 10;
		robustDistanceOutlierThreshold = 3.5;
	}
}HybridTrackerParam;

class HybridTracker : public RTracker{
public:
	HybridTracker(History *history);
	virtual void process(cv::Mat &_ref_frame, cv::Mat &_ref_features, cv::Mat &_prev_frame, cv::Mat &_prev_features, cv::Mat &_cur_frame, cv::Mat &cur_features, std::vector<bool> &valid);

protected:
	HybridTrackerParam param;
	History *history;
};

}


#endif
