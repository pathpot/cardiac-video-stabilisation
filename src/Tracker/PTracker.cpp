#include "PTracker.h"

void ps::PTracker::process(cv::Mat &_ref_frame, cv::Mat &_ref_features, cv::Mat &_prev_frame, cv::Mat &_prev_features, cv::Mat &_cur_frame, cv::Mat &cur_features, std::vector<bool> &valid){
	OpticalFlow::indTrack(_prev_frame, _cur_frame, _prev_features, cur_features, param.winSize, param.maxIter, param.maxLevel, valid, param.converge_eps, false);
}
