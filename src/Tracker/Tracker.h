#ifndef _PS_TRACKER_H_
#define _PS_TRACKER_H_

#include <opencv2/opencv.hpp>

namespace ps{
class Tracker{
public:
	virtual ~Tracker(){};
	virtual void process(cv::Mat &_ref_frame, cv::Mat &_ref_features, cv::Mat &_prev_frame, cv::Mat &_prev_features, cv::Mat &_cur_frame, cv::Mat &cur_features, std::vector<bool> &valid) = 0;
};


}

#endif
