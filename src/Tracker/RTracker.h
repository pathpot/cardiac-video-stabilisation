#ifndef _PS_RTracker_H_
#define _PS_RTracker_H_

#include "PTracker.h"
#include "../Tools/OpticalFlow/OpticalFlow.h"

namespace ps{
class RTracker : public PTracker{
public:
	virtual void process(cv::Mat &_ref_frame, cv::Mat &_ref_features, cv::Mat &_prev_frame, cv::Mat &_prev_features, cv::Mat &_cur_frame, cv::Mat &cur_features, std::vector<bool> &valid);
};

}


#endif
