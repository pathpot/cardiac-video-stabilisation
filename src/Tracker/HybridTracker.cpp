#include "HybridTracker.h"

ps::HybridTracker::HybridTracker(History *history){
	this->history = history;
}

void ps::HybridTracker::process(cv::Mat &_ref_frame, cv::Mat &_ref_features, cv::Mat &_prev_frame, cv::Mat &_prev_features, cv::Mat &_cur_frame, cv::Mat &cur_features, std::vector<bool> &valid){
	cv::Mat pTracker_features, rTracker_features;
	cur_features.copyTo(pTracker_features);
	cur_features.copyTo(rTracker_features);

	// P-Tracker and R-Tracker
	std::vector<bool> validR, validP;
	OpticalFlow::indTrack(_ref_frame, _cur_frame, _ref_features, rTracker_features, PTracker::param.winSize, PTracker::param.maxIter, PTracker::param.maxLevel, validR, PTracker::param.converge_eps, true);
	OpticalFlow::indTrack(_prev_frame, _cur_frame, _prev_features, pTracker_features, PTracker::param.winSize, PTracker::param.maxIter, PTracker::param.maxLevel, validP, PTracker::param.converge_eps, false);
	valid.clear();
	for(int i=0;i<validR.size();i++){
		valid.push_back(validR[i] || validP[i]);
	}

	//set robust stat
	cv::Mat X, T, S;

	cv::Mat ref_row, prev_row;
	bool ref_good = true, prev_good = true;
	double ncc_rTracker, ncc_pTracker;
	for(int i=0;i<cur_features.rows;i++){
		// compute NCC
		ncc_rTracker = PatchMeasure::computeNCC(_ref_frame, _ref_features.at<double>(i, 0), _ref_features.at<double>(i, 1),
				_cur_frame, rTracker_features.at<double>(i, 0), rTracker_features.at<double>(i, 1),
				PTracker::param.winSize);
		ncc_pTracker = PatchMeasure::computeNCC(_ref_frame, _ref_features.at<double>(i, 0), _ref_features.at<double>(i, 1),
				_cur_frame, pTracker_features.at<double>(i, 0), pTracker_features.at<double>(i, 1),
				PTracker::param.winSize);
		if(history->getFeatureHistory(X, i, param.mcdTrainFrame)){
			RobustStat::fastMCD(X, T, S, param.mcdInitN, param.mcdSelectN);

			// get estimated one
			ref_row = rTracker_features.row(i);
			prev_row = pTracker_features.row(i);

			// check outlier
			ref_good = !RobustStat::isOutLier(ref_row, T, S, param.robustDistanceOutlierThreshold);
			prev_good = !RobustStat::isOutLier(prev_row, T, S, param.robustDistanceOutlierThreshold);

			 if(ncc_rTracker >= ncc_pTracker){
				 if(ref_good){
					 cur_features.at<double>(i, 0) = rTracker_features.at<double>(i, 0);
					 cur_features.at<double>(i, 1) = rTracker_features.at<double>(i, 1);
				 }else{
					cur_features.at<double>(i, 0) = pTracker_features.at<double>(i, 0);
					cur_features.at<double>(i, 1) = pTracker_features.at<double>(i, 1);
				 }
			}else if(ncc_rTracker <= ncc_pTracker){
				if(prev_good){
					cur_features.at<double>(i, 0) = pTracker_features.at<double>(i, 0);
					cur_features.at<double>(i, 1) = pTracker_features.at<double>(i, 1);
				}else{
					cur_features.at<double>(i, 0) = rTracker_features.at<double>(i, 0);
					cur_features.at<double>(i, 1) = rTracker_features.at<double>(i, 1);
				}
			}
		}else{ //use NCC
			 if(ncc_rTracker > ncc_pTracker){
				cur_features.at<double>(i, 0) = rTracker_features.at<double>(i, 0);
				cur_features.at<double>(i, 1) = rTracker_features.at<double>(i, 1);
			}else{
				cur_features.at<double>(i, 0) = pTracker_features.at<double>(i, 0);
				cur_features.at<double>(i, 1) = pTracker_features.at<double>(i, 1);
			}
		}
	}


}

