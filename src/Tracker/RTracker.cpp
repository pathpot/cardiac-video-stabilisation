#include "RTracker.h"

void ps::RTracker::process(cv::Mat &_ref_frame, cv::Mat &_ref_features, cv::Mat &_prev_frame, cv::Mat &_prev_features, cv::Mat &_cur_frame, cv::Mat &cur_features, std::vector<bool> &valid){
	OpticalFlow::indTrack(_ref_frame, _cur_frame, _ref_features, cur_features, PTracker::param.winSize, PTracker::param.maxIter, PTracker::param.maxLevel, valid, PTracker::param.converge_eps, true);
}
