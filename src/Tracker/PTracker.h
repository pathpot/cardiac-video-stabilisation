#ifndef _PS_PTracker_H_
#define _PS_PTracker_H_

#include "Tracker.h"
#include "../Tools/OpticalFlow/OpticalFlow.h"

namespace ps{

typedef struct PTrackerParam{
	int winSize, maxIter, maxLevel, converge_eps;
	PTrackerParam(){
		printf("Set parameters for PTracker...\n");
		maxLevel = 5;
		maxIter = 10;
		converge_eps = 1;
		//TODO set window Size
		winSize = 15;//21;
	}
}PTrackerParam;

class PTracker : public Tracker{
public:
	virtual void process(cv::Mat &_ref_frame, cv::Mat &_ref_features, cv::Mat &_prev_frame, cv::Mat &_prev_features, cv::Mat &_cur_frame, cv::Mat &cur_features, std::vector<bool> &valid);

protected:
	PTrackerParam param;
};

}


#endif
